const koa = require('koa');
const logger = require('koa-logger');
const compress = require('koa-compress');
const conditional = require('koa-conditional-get');
const etag = require('koa-etag');
const bodyParser = require('koa-bodyparser');
const router = require('./api/routes');
const chalk = require('chalk');
const static = require('koa-static');

const port = process.env.PORT || 3000;

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/learn");

const app = new koa();

app.use(static('./public'));
app.use(conditional());
app.use(etag());
app.use(compress());
app.use(bodyParser({
    formLimit: '7mb'
}));

app.use(logger());
app.use(router.routes());
app.use(router.allowedMethods());

const server = app.listen(port, () => console.log(chalk.black.bgGreen.bold(`Listening on port ${port}`)));

const WebSocket = require('socket.io');

const wss = WebSocket.listen(server);

wss.on('connection', (ws) => {
    ws.on('public_chat', (msg) => {
        ws.emit('public_chat', msg);
    });

});

module.exports = app;