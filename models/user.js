const mongoose = require('mongoose');
const {Schema} = mongoose;
const uuid = require('uuid');

module.exports = function() {
    const schema = new Schema({
        _id: {
            type: String,
            default: uuid,
        },

        _version: {
            type: Number,
            default: 1.0,
        },

        login: {
            type: String,
            required: true,
        },

        password: {
            type: String,
            required: true,
        },

        createDate: {
            type: Date,
            default: () => (new Date()),
        },
    });


    return mongoose.model('User', schema);
};