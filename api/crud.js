let self; 

module.exports = class Crud{
    constructor(model) {
        this.model = model;
        self = this;
    }

    async create (ctx, next) {
        ctx.body = await self.model.create({name: 'me'});
        await next();
    }

    async list (ctx, next) {
        ctx.body = await self.model.find().lean();
        await next();
    }

    async show (ctx, next) {

        await next();
    }

    async update (ctx, next)  {

        await next();
    }

    async delete (ctx, next) {

        await next();
    }

}