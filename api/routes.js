const Router = require('koa-router');
const User = require('./users');
const PagesController = require('./pages');
const pages = new PagesController();
const userModel = require('../models/user');
const users = new User( new userModel() );


const everyone = new Router()
    .get('/', pages.main)

    .get('/register', pages.register)
    .get('/auth', pages.auth)

    .get('/chat', pages.chat)

    .post('/register', users.register)
    .post('/auth', users.auth);


module.exports = new Router()
    .use(everyone.routes());
