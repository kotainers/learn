const Crud = require('./crud');
let self; 

module.exports = class UserCtrl extends Crud {
    constructor(model) {
        super(model);
        this.model = model;
        self = this;
    }

    async register (ctx, next){
        ctx.body = await self.model.create(ctx.request.body);
        await next();
    }

    async auth (ctx, next){
        const user = await self.model.findOne({login: ctx.request.body.login, password: ctx.request.body.password});
        if (user) {
            ctx.body = user;
            ctx.redirect('./chat')
        } else {
          ctx.status = 401;
        }
        await next();
    }

}
