const pug = require('pug');


module.exports = class PagesController {
    async main (ctx, next){
        ctx.body = await pug.renderFile('./views/index.pug');
        await next();
    }

    async register (ctx, next){
        ctx.body = await pug.renderFile('./views/register.pug');
        await next();
    }

    async auth (ctx, next){
        ctx.body = await pug.renderFile('./views/auth.pug');
        await next();
    }

    async chat (ctx, next){
        ctx.body = await pug.renderFile('./views/publicchat.pug');
        await next();
    }
}
