$(function () {
    var ws = io();

    ws.on('connect', function(msg) {
        ws.emit('init', 'hi');
    });

    ws.on('public_chat', function(msg) {
        console.log('msg');
        $('#messages').append($('<li>').text(msg));
        window.scrollTo(0, document.body.scrollHeight);
    });

    $('form').submit(function(){
        ws.emit('public_chat', $('#m').val());
        $('#m').val('');
        return false;
    });

});

